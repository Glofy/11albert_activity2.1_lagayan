import React from 'react';
import logo from './logo.png';
import HELLO from './HELLO.png';
import king from './king.png';
import queen from './queen.png';
import halloween from './halloween.png'

import './App.css';

 function App() {
     return (
     <html className="myhtml">
     <header className="apple" >
      <div className="gamemaster">
        <div className="player_one">
          <hr />
          <img src={logo} class="bear" width="164px" />
          <img src={HELLO} class="rotate"  />
        </div>
        <div className="player_two">

          <div className="bodymass">
            <font size="9" >
            What is a Chess Gambit?</font><br /> A chess gambit is an opening where you oftenly sacrifice a pawn to get more control of the center of the chess board.
            <p/>There are several ways to sacrifice a pawn to gain control of the center of the board, in other words there are several gambits and countergambits out there but we will only choose some of the most common, aggresive and dangerous gambits to watch out for.
          </div>
        </div>
      </div>
     </header>
     <body>
      <div className="mysexybody">
      <div classname="bodymaster"> <hr />  
         <font size="5" ><b>The King's Gambit</b></font><hr /><br /><br />
          For my personal experience and preference, the King's Gambit is one of the most dangereous and offensive way to play the game. In the Kings Gambit both kings will be exposed to some serious threats and mating attacks,<br /> one wrong move that is out of the variation will cause tremendous damage.
          This is an offensive oppening for white where is starts with E4, F5 and then you sacrifice your pawn on F4. If you are playing white I advise you to memorize all the possible<br /> variations first before playing this opening because you will be completely destroyed if you dont know how to play this against a decent player. 
          And if you are playing black I advise you NOT to take it and play safe because taking it will both expose your kings, yes white may have the advantage but if you memorize the variations the most safest outcome for you both is your kings coming up to the center of the board, in other words if you see your opponent doing this oppening to you, I advise you not to take that free pawn and play safe but you can take it if you want to have a dangerous action packed game.Speaking of variations there are some called the King's Gambit declined, a line or game where you dont accept the gambit.  <br /><br /> If you are interested and if you want to learn this opening and how to counter it here are some videos that will help you learn some variations. <br /> <br />
        <div className="gamemaster">
          <div className="player_three">
          <img src={king} class="king" width="400px"/>
          </div>
          <div className="player_four">
          <iframe width= "364" height="256" src ="https://www.youtube.com/embed/n5ImJ2row6E"/>
          </div>
          <div className="player_five">
          <iframe width= "364" height="256" src ="https://www.youtube.com/embed/YFydlMqz3gk"/>
          </div>
        </div>
      </div>
     </div>
     <div className="myslimbody"><hr /> 
      <font size="5" ><b>The Queen's Gambit</b></font><hr /><br /><br />
      If we have a King's Gambit we also have a Queen's Gambit. The Queen's Gambit is quite popular nowadays due to the fact that it has it is also named after a famous Netflix series. The Queen's gambit is a solid opening<br /> where you open up the two queen sides found in the board. Unlike the King's Gambit where you hunt down the king as fast as possible, the Queen's gambit offers a safe game where you simply develop your peices and end the game smoothly. Accepting the gambit doesn't really affects any odds of you to win but if you are white I must warn you that there is a line called Queen's Gambit declined which is well respected and somehow is still beleived to be unbeatable up to this date. This gambit starts with the moves d4, d5 then c4 and e6. <br /> <br />
      If you are interested and if you want to learn this opening and how to counter it here are some videos that will help you learn some variations.<br /> <br />
      <div className="gamemaster">
       <div className="player_six">
       <img src={queen} class="king" width="400px"/>
       </div><div className="player_seven">
          <iframe width= "364" height="256" src ="https://www.youtube.com/embed/HdHWAuQRG7E"/>
          </div>
          <div className="player_eight">
           <iframe width= "364" height="256" src ="https://www.youtube.com/embed/JLQtOWt4Jzs"/>
          </div>
        </div>
       </div>
         <div className="myhealthybody"><hr /> 
          <font size="5" ><b>The Halloween Gambit</b></font><hr /><br /><br />
           One of the most odd gambits out there is the Halloween Gambit where you or your opponent will sacrifice a knight just to gain control of the center and to eventually get his knight back. This opening is tricky and dangerous, I advise you to stay away from this opening and avoid it. Although yes taking the knight or accepting the gambit will be a horse for a pawn and it seems like you have won the trade, believe me it really depends because if you play black white will eventually get his horse back or ferouciously control the center of the board and will leave no space for your peices to move around thus setting them back and leading you to be left behind in development of peices as the game progresses. This opening starts with  e4 e5,then Nf3 Nc6 Nc3 and Nf6 Nxe5.<br /> <br />
            If you are interested and if you want to learn this opening and how to counter it here are some videos that will help you learn some variations.<br /><br />
             <div className="gamemaster">
             <div className="player_nine">
             <img src={halloween} class="king" width="400px"/>
               </div><div className="player_ten">
                 <iframe width= "364" height="256" src ="https://www.youtube.com/embed/roz1YeJjvUY"/>
                  </div>
              <div className="player_eleven">
             <iframe width= "364" height="256" src ="https://www.youtube.com/embed/DajxH6xcaPQ"/>
           </div>
        </div>
      </div>
    </body>
    <footer className="myendlessbody">
        By: Ralph Matthew Glofien O. Lagayan<br /> 11-St. Albert
    </footer>
  </html>
  );
}

export default App;
